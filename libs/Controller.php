<?php
/**
 * Created by PhpStorm.
 * User: ngg
 * Date: 2016.03.22.
 * Time: 10:44
 */

class Controller
{

    function __construct()
    {
        //echo 'Main controller<br />';
        $this->view = new View();

    }

    public function loadModel($name)
    {

       $path = MODELS . $name . '_model.php';


        if (file_exists($path)) {

            require MODELS . $name . '_model.php';


            $modelName = $name . '_model';
            $this->model = new $modelName();
        }/*
        else {
            echo '<br/>nem létezik a következő model: ' . $path . '<br/>';
        }*/
    }

}