<?php
/**
 * Created by PhpStorm.
 * User: ngg
 * Date: 2016.03.22.
 * Time: 10:46
 */

class Session
{

    public static function init()
    {
        @session_start();
    }

    public static function set($key, $value, $isSerialize = false)
    {
        if ($isSerialize) {
            $_SESSION[$key] = serialize($value);
        } else {
            $_SESSION[$key] = $value;
        }
    }

    public static function get($key, $isSerialize = false)
    {

        if (isset($_SESSION[$key])) {

            if ($isSerialize) {
                return unserialize($_SESSION[$key]);
            } else {
                return $_SESSION[$key];
            }
        }
    }

    public static function destroy()
    {
        //unset($_SESSION);
        session_destroy();
    }


}