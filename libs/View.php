<?php
/**
 * Created by PhpStorm.
 * User: ngg
 * Date: 2016.03.22.
 * Time: 10:45
 */


class View
{

    function __construct()
    {
        //echo 'this is the view';
    }

    public function render($name, $noInclude = false)
    {
        if ($noInclude == true) {
            require VIEWS . $name . '.php';
        } else {
            require VIEWS . 'header.php';
            require VIEWS . $name . '.php';
            require VIEWS . 'footer.php';
        }
    }

}