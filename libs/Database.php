<?php
/**
 * Created by PhpStorm.
 * User: ngg
 * Date: 2016.03.22.
 * Time: 11:19
 */

class Database extends PDO
{

    public function __construct($DB_TYPE, $DB_HOST, $DB_NAME, $DB_USER, $DB_PASS)
    {
        //echo 'connection';

        try {
            parent::__construct($DB_TYPE . ':host=' . $DB_HOST . ';dbname=' . $DB_NAME, $DB_USER, $DB_PASS,
                array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
            $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->setAttribute(PDO::ATTR_ORACLE_NULLS, PDO::NULL_TO_STRING);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }


    }


    public function selectSQL($sql, $array = array(), $singleFetch = false, $limited = false ,$fetchMode = PDO::FETCH_ASSOC)
    {
        $sth = $this->prepare($sql);


        if(!$limited){

            foreach ($array as $key => $value) {
                $sth->bindValue(":$key", $value);
            }

        }

        //var_dump($sth);
        $sth->execute();



        if ($singleFetch) {
            return $sth->fetch($fetchMode);
        } else {
            return $sth->fetchAll($fetchMode);
        }
    }



    public function insertSQL($table, $data)
    {
        // ksort($data);

        $fieldNames = implode('`, `', array_keys($data));
        // echo "fieldnames: (". $fieldNames .')<br />';
        $fieldValues = ':' . implode(', :', array_keys($data));
        // echo "fieldvalues: (". $fieldValues .')<br />';

        $sth = $this->prepare("INSERT INTO $table (`$fieldNames`) VALUES ($fieldValues)");

        foreach ($data as $key => $value) {
            $sth->bindValue(":$key", $value);
        }

        //print_r($sth);
        // echo '<br/>';
        $sth->execute();
    }

    public function update($table, $data, $where)
    {

        $fieldDetails = NULL;

        foreach ($data as $key => $value) {
            $fieldDetails .= "`$key`= $value,";

        }
        $fieldDetails = rtrim($fieldDetails, ',');


        $whereDetails = null;

        foreach($where as $key => $value){
            $whereDetails .= "`$key` = $value AND";
        }

        $whereDetails = rtrim($whereDetails, ' AND');


        $sth = $this->prepare("UPDATE $table SET $fieldDetails WHERE $whereDetails");

        foreach ($data as $key => $value) {
            $sth->bindValue(":$key", $value);
        }

        foreach ($where as $key => $value) {
            $sth->bindValue("$key", $value);
        }


        $sth->execute();
    }

    public function deleteSQL($table, $where, $limit = 1)
    {
        return $this->exec("DELETE FROM $table WHERE $where LIMIT $limit");
    }


}