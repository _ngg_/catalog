<?php
/**
 * Created by PhpStorm.
 * User: ngg
 * Date: 2016.03.22.
 * Time: 10:43
 */


class Bootstrap
{

    function __construct()
    {

        $url = isset($_GET['url']) ? $_GET['url'] : null;
        $url = rtrim($url, '/');
        $url = explode('/', $url);
        //print_r($url);

        if (empty($url[0])) {

            require CONTROLLERS . 'Home.php';

            $controller = new Home();
            $controller->loadModel('home');
            $controller->index();

            return true;
        }

        $file = CONTROLLERS . $url[0] . '.php';
        if (file_exists($file)) {

            require $file;
        } else {
            $this->error();
        }

        //model betöltése

        $controller = new $url[0];
        $controller->loadModel($url[0]);

        // metódus hívás
        if (isset($url[3])) {
            if (method_exists($controller, $url[1])) {


                $controller->{$url[1]}($url[2],$url[3]);

            } else {
                $this->error();
            }
        } else
            if (isset($url[2])) {
                if (method_exists($controller, $url[1])) {

                    $controller->{$url[1]}($url[2]);

                } else {
                    $this->error();
                }
            } else {
                if (isset($url[1])) {

                    if (method_exists($controller, $url[1])) {

                        $controller->{$url[1]}();

                    } else {
                        $this->error();
                    }
                } else {

                    $controller->index();
                }
            }


    }

    function error()
    {
        require CONTROLLERS . 'error.php';
        $controller = new Error();
        $controller->index();
        return false;
    }

}