<?php
/**
 * Created by PhpStorm.
 * User: ngg
 * Date: 2016.03.25.
 * Time: 22:55
 */

class Auth {


    static function check(){

        Session::init();

        $user = Session::get('user', true);
        $loggedIn = Session::get('loggedIn');

        if(!$loggedIn || $user == null || $user->getRights() != Rights::ADMIN){

            Session::destroy();
            header('Location:' . URL . 'login');

        }

    }



}