<?php
/**
 * Created by PhpStorm.
 * User: ngg
 * Date: 2016.03.23.
 * Time: 15:46
 */

class Debug {

    static function vdump($variables = array()) {

        foreach ($variables as $v) {

            echo '<br /><hr />';
            var_dump($v);
        }


        die(); exit();
    }


    static function printr($variables = array()) {

        foreach ($variables as $v) {

            echo '<br /><hr />';
            print_r($v);
        }


        die(); exit();
    }
}