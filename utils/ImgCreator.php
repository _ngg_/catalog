<?php

/**
 * Created by PhpStorm.
 * User: ngg
 * Date: 2016.03.25.
 * Time: 19:57
 */
class ImageCreator
{


    static function generate()
    {
        $counter = 0;

        for ($i = 1; $i <= 25; $i++) {

            for ($j = 1;$j <= 5;$j++) {

                $counter++;

                // Create a blank image and add some text
                $im = imagecreatetruecolor(100, 150);

                $text_color = imagecolorallocate($im, 200, 140, 190);

                imagestring($im, 25, 0, 0, ('Konyv_' . $i . '.' . $j), $text_color);

                imagejpeg($im, "public/img/" . $counter . ".jpg");

                // Free up memory
                imagedestroy($im);
            }

        }
    }
}