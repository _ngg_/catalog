<?php
/**
 * Created by PhpStorm.
 * User: ngg
 * Date: 2016.03.22.
 * Time: 10:38
 */

/* PATHS */
define('URL', 'http://localhost/catalog/');

define('PROJECT_PATH', getcwd() . '/catalog/');
define('LIBS', 'libs/');
define('UTILS', 'utils/');
define('CONTROLLERS', 'controllers/');
define('MODELS', 'models/');
define('VIEWS', 'views/');
define('IMAGES', URL .'public/img/');
define('JAVASCRIPT', '/js/default.js');

/* DATABASE */
define('DB_TYPE', 'mysql');
define('DB_HOST', 'localhost');
define('DB_NAME', 'catalog');
define('DB_USER', 'root');
define('DB_PASS', '');