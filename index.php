<?php
/**
 * Created by PhpStorm.
 * User: ngg
 * Date: 2016.03.22.
 * Time: 10:38
 */

require_once 'Config.php';
require_once 'libs/Bootstrap.php';

function __autoload($class)
{
    if (file_exists(LIBS . $class . '.php')) {
        require LIBS . $class . ".php";
    }

    if (file_exists(MODELS . $class . '.php')) {
        require MODELS . $class . ".php";
    }

    if (file_exists(UTILS . $class . '.php')) {
        require UTILS . $class . ".php";
    }
}

$boots = new Bootstrap();