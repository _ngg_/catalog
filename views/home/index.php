<?php
/**
 * Created by PhpStorm.
 * User: ngg
 * Date: 2016.03.22.
 * Time: 11:13
 */

// DISPLAY -->


echo '<div id="itemsContent">';

echo '<h1>Katalógus</h1>';


foreach ($this->items as $item) {

    echo '<div class="item">';

    itemName($item);

    itemImage($item);

    itemPrice($item);

    itemDetails($item);

    echo '</div>';

}
echo '</div>';


echo '<div id="navigation">';

navigation($this);

echo '</div>';

// <-- DISPLAY

// FUNCTIONS FOR DISPLAY


function itemName($item)
{
    echo '<b>' . $item->getName() . '</b><br />';
}

function itemImage($item)
{
    echo '<img class="cover" src="' . IMAGES . $item->getCoverImage() . '" title="Termék képe"/><br/>';

}

function itemPrice($item)
{
    if (($item->getDiscount() != null && $item->getDiscount() > 0)
        && date($item->getDiscountExpireDate()) > date('Y-m-d H:i:s')
    ) {
        //echo $item->getDiscount(). '---' .$item->getDiscountExpireDate().'<br />';

        echo '<del>' . $item->getPrice() . '</del> Ft helyett <label class="discountPrice">' .
            round($item->getPrice() * (1 - ($item->getDiscount() / 100))) . '</label> Ft<br/>' .
            $item->getDiscount() . ' % kedvezmény';

    } else {

        echo '<p>'.$item->getPrice() . ' Ft</p><br/>';
    }
}

function itemDetails($item)
{
    echo '<a href="' . URL . 'home/item/' . $item->getId() . '"> Részletek</a>';
}


function navigation($view)
{

    if ($view->bottom >= $view->stepSize) {

        if ($view->items != null && sizeof($view->items) == $view->stepSize) {

            echo '<a id="prev" class="prev" href="';

        } else {
            echo '<a id="prev" href="';
        }
        echo URL . 'home/index/' . ($view->bottom - $view->stepSize) . '">Előző oldal</a>';
    }
    if ($view->items != null && sizeof($view->items) == $view->stepSize) {

        echo '<a id="next" href="' . URL . 'home/index/' . ($view->bottom + $view->stepSize) . '">Következő oldal</a>';
    }

}

?>