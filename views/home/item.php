<?php
/**
 * Created by PhpStorm.
 * User: ngg
 * Date: 2016.03.23.
 * Time: 22:02
 */

// DISPLAY -->

if ($this->message != null) {
    echo $this->message;
} else {


    echo '<div id="itemDetails">';

    itemName($this->item->getName());


    echo '<div id="itemDetailsImg">';

    itemImg($this->item->getCoverImage());

    echo '</div>';

    echo '<div id="itemInfo">';

    itemPrice($this->item->getDiscount(), $this->item->getDiscountExpireDate(), $this->item->getPrice());

    itemStatus($this->item->getState());

    itemDetails($this->item->getDetails());
    echo '</div>';

    itemImages($this->item->getImages());


      echo '</div>';
}

// <-- DISPLAY

// FUNCTIONS FOR DISPLAY

function itemName($name)
{
    echo '<p><b>Termék neve: ' . $name . '</b></p>';
}

function itemImg($coverImage)
{
    echo '<img class="coverDetail" src="' . IMAGES . $coverImage . '" title="Termék képe" id="cover"/>';
}

function itemPrice($discount, $discountExpireDate, $price)
{

    if (($discount != null && $discount > 0)
        && date($discountExpireDate) > date('Y-m-d H:i:s')
    ) {

        echo '<u>Ár:</u> <del>' . $price . '</del> Ft helyett ' .
            round($price * (1 - ($discount / 100))) . ' Ft<br/>' .
            '<u>Kedvezmény:</u> '.$discount .'%<br />';

        echo '<u>A kedvezmény eddig tart:</u> ' .
            date_format(date_create($discountExpireDate), "Y.m.d H:i") . '-ig.<br />';

    } else {

        echo '<u>Ár: </u>'.$price . ' Ft<br />';
    }

}


function itemDetails($details)
{
    echo '<u>Termék leírás:</u> ' . $details;
}


function itemStatus($status)
{

    echo '<u>A termék státusza:</u> ';
    switch ($status) {
        case Status::RAKTARON :
            echo 'Raktáron.';
            break;
        case Status::ELFOGYOTT :
            echo 'Átmenetileg nincs raktáron.';
            break;
        case Status::KIFUTOTT :
            echo 'Kifutott termék, nem áruljuk többé.';
            break;
        default :
            echo 'Forduljon ügyfélszolgálatunkhoz.';
    }

    echo '<br />';
}

function itemImages($images)
{
    echo '<div id="otherImages"><u>További képek:</u><br/>';

    if (sizeof($images) > 1) {


        foreach ($images as $img) {
            echo '<img class="thumbImg" src="' . IMAGES . $img . '" alt="Nincs meg a kép" id="' . $img . '"/>';
        }


    } else {
        echo 'Nincsen.';
    }
    echo '</div>';
}

?>