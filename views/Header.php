<!DOCTYPE html>
<html lang="hu">
<head>

    <meta charset="UTF-8">
    <meta name="description" content="Shop">
    <meta name="keywords" content="szuper, akció, szuper akció, shoping, shop">
    <meta name="author" content="ngg">

    <title>Katalógus Alkalmazás</title>

    <link rel="stylesheet" href="<?php echo URL; ?>public/css/default.css"/>
    <link rel="stylesheet" href="<?php echo URL; ?>public/css/jquery-ui.css"/>
    <noscript>
        Az oldal használatához szükség van a Javascriptre, kérlek aktiváld.
        <style>div { display:none; }</style>
    </noscript>
    <script type="text/javascript" src="<?php echo URL; ?>public/js/jquery-2.2.1.min.js"></script>

    <?php
    if (isset($this->js)) {

        foreach ($this->js as $js) {
            echo '<script type="text/javascript" src="'
                . URL . 'views/' . $js . '"></script>';
        }
    }

    ?>
</head>
<body>

<?php Session::init();
$loggedIn = Session::get('loggedIn');
$user = Session::get('user',true);
?>

<div id="wrap">

    <div id="header">
             <div id="leftUpCorner"></div>
            <a id="homeAhref" class="homeAhref" href="<?php echo URL; ?>home">Katalógus</a>
            <?php if ($loggedIn == true) { ?>
                <a href="<?php echo URL; ?>dashboard">Adminisztrációs felület</a>
                <a href="<?php echo URL; ?>login/logout">Kilépés</a>

                <div id="welcome">Üdvözöllek <?php  echo $user->getName();
                 ?>!</div>
            <?php } else { ?>

                <a href="<?php echo URL; ?>login">Bejelentkezés</a>

            <?php } ?>


    </div>



    <div id="content">