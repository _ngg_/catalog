<?php
/**
 * Created by PhpStorm.
 * User: ngg
 * Date: 2016.03.22.
 * Time: 22:07
 */

?>


<div id="dashboard">

    <div id="dashboardItems">

    </div>

    <div id="newItemFormDiv">

        <form id="newItemForm" method="post">
            <p><b>Termékfelvitel</b></p>
            <p>
                <label>Termék neve: </label>
                <input type="text" name="name" id="newItemFormName"
                       required title="Termék neve.">
            </p>
            <p>
                <label>Termék ára: </label>
                <input type="number" name="price" id="newItemFormPrice"
                       min="1" value="1" required title="Termék ára.">
            </p>
            <p>
                <label>Kedvezmény: </label>
                <input type="number" name="discount" id="newItemFormDiscount"
                       min="0" max="99" value="0" title="Termék kedvezmény %-ban.">
            </p>
            <p>
                <label>Termék kedvezményének lejárati ideje:</label>
                <input type="text" id="newItemFormDate" name="discount_expire_date"
                       title="Kérem adja meg a kedvezményes időszak lejárati idejét!" readonly="true"">
            </p>
            <p>
                <label>Termék leírása:<br/></label>
                <textarea name="details" id="newItemFormDetails" rows="10" cols="40"></textarea>
            </p>
            Termék képek, a legelső a borító.<br/>
            <input name="image1" id="newItemFormImage1" type="file" required
                   title="Egy kép legalább szükséges borítónak."/>
            <input name="image2" id="newItemFormImage2" type="file"/>
            <input name="image3" id="newItemFormImage3" type="file"/>
            <input name="image4" id="newItemFormImage4" type="file"/>
            <input name="image5" id="newItemFormImage5" type="file"/>
            <br />
            <p><input type="submit" value="Termék adatainak feltöltése"></p>

        </form>


    </div>

</div>