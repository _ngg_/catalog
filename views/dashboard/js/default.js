/**
 * Created by ngg on 2016.03.25..
 */

$(document).ready(function () {


    getItems();

    datePickerSwap();


    $("#newItemFormDiscount").change(function () {

        var t = $("#newItemFormDiscount").val();

        if (t == 0) {

            $("#newItemFormDate").prop('readonly', true);

        }
        else {
            $("#newItemFormDate").prop('readonly', false);

        }
    });


    function getItems() {

        $.get(("dashboard/itemsHttp"), function (result, status) {


            writeItemPanel(result);

        }, 'json');

    }


    function writeItemPanel(items) {

        var _out = '<table>' +
            '<tr>' +
            '<td>Id</td>' +
            '<td>Név</td>' +
            '<td>Ár</td>' +
            '<td>Kedv. %</td>' +
            '<td>Ideje</td>' +
            '<td>Részletek</td>' +
            '<td>Állapot</td>' +
            '</tr>';

        var counter = 1;
        for (var i = 0; i < items.length; i++) {

            var item = '<tr>';
            item += tableColoring(counter, true);
            item += items[i].id;
            item += tableColoring(counter, false);

            item += tableColoring(counter, true);
            item += '<b>' + items[i].name + "</b> ";
            item += tableColoring(counter, false);

            item += tableColoring(counter, true);
            item += items[i].price + ' Ft';
            item += tableColoring(counter, false);

            item += tableColoring(counter, true);
            item += items[i].discount;
            item += tableColoring(counter, false);

            item += tableColoring(counter, true);
            item += items[i].discount_expire_date.substring(0, 10) + '</td>';
            item += tableColoring(counter, false);

            var details = tableColoring(counter, true);
            details += items[i].details.substring(0, 40)+'...';
            details += tableColoring(counter, false);

            item += details;


            item += tableColoring(counter, true);
            switch(items[i].state){
                case "inStock" : item += "Raktáron"; break;
                case "soldOut" : item += "Elfogyott"; break;
                case "ranOut" : item += "Kifutott"; break;
                default : item+= "Ismeretlen.";
            }
            item += tableColoring(counter, false);

            item += '</tr>';
            _out += item;

            counter++;
        }

        _out += '</table>';


        $("#dashboardItems").html(_out);


    }

    function tableColoring($n, isBeginner) {
        if ($n % 2 == 0) {
            if (isBeginner) {
                return '<td>';
            } else {
                return '</td>';
            }
        }
        else {
            if (isBeginner) {
                return '<th>';
            }
            else {
                return '</th>'
            }
        }
    }

    /**
     * Elküldi az új termék felvételéhez szükséges adatokat.
     */
    $("form#newItemForm").submit(function () {

        var formData = new FormData($(this)[0]);

        //console.log(formData);

        $.ajax({
            url: 'dashboard/insertHttp',
            type: 'POST',
            data: formData,
            async: false,
            success: function (data) {
                //alert(data)
            },
            cache: false,
            contentType: false,
            processData: false
        });

        //a termék adatok frissítése, újbóli lekérés
        getItems();

        //a form elemeit reset-eli.
        $(this).closest("form#newItemForm").get(0).reset();

        return false;
    });


    function datePickerSwap() {
        $("#newItemFormDate").datepicker({
            dateFormat: 'yy-mm-dd'
        });

    }
});