-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.9-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Verzió:              9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for catalog
DROP DATABASE IF EXISTS `catalog`;
CREATE DATABASE IF NOT EXISTS `catalog` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `catalog`;


-- Dumping structure for tábla catalog.image
DROP TABLE IF EXISTS `image`;
CREATE TABLE IF NOT EXISTS `image` (
  `item_id` int(11) NOT NULL,
  `image` varchar(45) DEFAULT NULL,
  KEY `fk_images_item_idx` (`item_id`),
  CONSTRAINT `fk_images_item` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table catalog.image: ~45 rows (approximately)
/*!40000 ALTER TABLE `image` DISABLE KEYS */;
INSERT INTO `image` (`item_id`, `image`) VALUES
	(1, '1.jpg'),
	(1, '2.jpg'),
	(1, '3.jpg'),
	(1, '4.jpg'),
	(1, '5.jpg'),
	(3, '11.jpg'),
	(3, '13.jpg'),
	(3, '14.jpg'),
	(2, '6.jpg'),
	(2, '7.jpg'),
	(2, '8.jpg'),
	(83, '83_0.jpg'),
	(83, '83_1.jpg'),
	(83, '83_2.jpg'),
	(83, '83_3.jpg'),
	(84, '84_0.jpg'),
	(84, '84_1.jpg'),
	(84, '84_2.jpg'),
	(84, '84_3.jpg'),
	(84, '84_4.jpg'),
	(85, '85_0.jpg'),
	(85, '85_1.jpg'),
	(85, '85_2.jpg'),
	(85, '85_3.jpg'),
	(85, '85_4.jpg'),
	(86, '86_0.jpg'),
	(86, '86_1.jpg'),
	(86, '86_2.jpg'),
	(86, '86_3.jpg'),
	(86, '86_4.jpg'),
	(87, '87_0.jpg'),
	(87, '87_1.jpg'),
	(88, '88_0.jpg'),
	(88, '88_1.jpg'),
	(88, '88_2.jpg'),
	(89, '89_0.jpg'),
	(89, '89_1.jpg'),
	(90, '90_0.jpg'),
	(91, '91_0.jpg'),
	(91, '91_1.jpg'),
	(93, '93_0.jpg'),
	(93, '93_1.jpg'),
	(94, '94_0.jpg'),
	(94, '94_1.jpg'),
	(94, '94_2.jpg'),
	(95, '95_0.jpg'),
	(95, '95_1.jpg'),
	(95, '95_2.jpg'),
	(95, '95_3.jpg'),
	(95, '95_4.jpg'),
	(96, '96_0.jpg'),
	(96, '96_1.jpg');
/*!40000 ALTER TABLE `image` ENABLE KEYS */;


-- Dumping structure for tábla catalog.item
DROP TABLE IF EXISTS `item`;
CREATE TABLE IF NOT EXISTS `item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `discount` int(11) DEFAULT NULL,
  `discount_expire_date` datetime DEFAULT NULL,
  `details` mediumtext,
  `state` enum('inStock','soldOut','ranOut') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;

-- Dumping data for table catalog.item: ~13 rows (approximately)
/*!40000 ALTER TABLE `item` DISABLE KEYS */;
INSERT INTO `item` (`id`, `name`, `price`, `discount`, `discount_expire_date`, `details`, `state`) VALUES
	(1, 'Könyv1', 1000, 10, '2016-04-11 15:45:35', '<b>Könyv 1 cím</b>\r\nNunquam dignus idoleum.\r\nCur historia resistere?\r\n<u>Ubi est fortis frondator?</u>\r\nDevatios credere, tanquam azureus clinias.\r\n<br />\r\n<i>Bursas ortum in grandis gandavum!</i>\r\nAltus, emeritis hibridas aliquando attrahendam de velox, raptus sectam.', 'inStock'),
	(2, 'Könyv2', 2000, 20, '2016-04-11 15:45:35', '<b>Könyv 2 cím</b>\r\nNunquam dignus idoleum.\r\nCur historia resistere?\r\n<u>Ubi est fortis frondator?</u>\r\nDevatios credere, tanquam azureus clinias.\r\n<br />\r\n<i>Bursas ortum in grandis gandavum!</i>\r\nAltus, emeritis hibridas aliquando attrahendam de velox, raptus sectam.', 'inStock'),
	(3, 'Könyv3', 3000, 30, '2016-04-11 15:45:35', '<b>Könyv 3 cím</b>\r\nNunquam dignus idoleum.\r\nCur historia resistere?\r\n<u>Ubi est fortis frondator?</u>\r\nDevatios credere, tanquam azureus clinias.\r\n<br />\r\n<i>Bursas ortum in grandis gandavum!</i>\r\nAltus, emeritis hibridas aliquando attrahendam de velox, raptus sectam.', 'inStock'),
	(83, 'Könyv4', 1500, 0, NULL, '<b>Könyv 4 cím</b>Nunquam dignus idoleum.<br/>Cur historia resistere?<br/><u>Ubi est fortis frondator?</u><br/>Devatios credere, tanquam azureus clinias.<br/><br/><i>Bursas ortum in grandis gandavum!</i><br/>Altus, emeritis hibridas aliquando attrahendam de velox, raptus sectam.', 'inStock'),
	(84, 'Könyv5', 1500, 20, '2016-04-28 00:00:00', '<b>Könyv 5 cím</b>Nunquam dignus idoleum.<br/>Cur historia resistere?<br/><u>Ubi est fortis frondator?</u><br/>Devatios credere, tanquam azureus clinias.<br/><br/><i>Bursas ortum in grandis gandavum!</i><br/>Altus, emeritis hibridas aliquando attrahendam de velox, raptus sectam.', 'inStock'),
	(85, 'Könyv6', 3333, 10, '2016-04-28 00:00:00', '<b>Könyv 6 cím</b>Nunquam dignus idoleum.<br/>Cur historia resistere?<br/><u>Ubi est fortis frondator?</u><br/>Devatios credere, tanquam azureus clinias.<br/><br/><i>Bursas ortum in grandis gandavum!</i><br/>Altus, emeritis hibridas aliquando attrahendam de velox, raptus sectam.', 'inStock'),
	(86, 'Könyv7', 5550, 99, '2016-04-28 00:00:00', '<b>Könyv 7 cím</b>Nunquam dignus idoleum.<br/>Cur historia resistere?<br/><u>Ubi est fortis frondator?</u><br/>Devatios credere, tanquam azureus clinias.<br/><br/><i>Bursas ortum in grandis gandavum!</i><br/>Altus, emeritis hibridas aliquando attrahendam de velox, raptus sectam.', 'inStock'),
	(87, 'Könyv8', 9999, 1, '2016-08-31 00:00:00', '<b>Könyv 8 cím</b><br/>Nunquam dignus idoleum.<br/>Cur historia resistere?<br/><u>Ubi est fortis frondator?</u><br/>Devatios credere, tanquam azureus clinias.<br/><i>Bursas ortum in grandis gandavum!</i><br/>Altus, emeritis hibridas aliquando attrahendam de velox, raptus sectam.', 'inStock'),
	(88, 'Könyv9', 10000, 0, NULL, '<b>Könyv 9 cím<br/>Nunquam dignus idoleum.<br/>Cur historia resistere?<br/><u>Ubi est fortis frondator?</u><br/>Devatios credere, tanquam azureus clinias.<br/><i>Bursas ortum in grandis gandavum!</i><br/>Altus, emeritis hibridas aliquando attrahendam de velox, raptus sectam.', 'inStock'),
	(89, 'Könyv10', 1111, 0, NULL, 'Könyv10<br/>Valami latin szöveg.', 'inStock'),
	(90, 'Könyv11', 1000, 0, NULL, 'Könyv11<br/><i>Valami</i>', 'inStock'),
	(91, 'Könyv12', 7777, 10, '2016-03-08 00:00:00', 'Könyv12<br/><b>Akciós volt 2016-03-08-ig</b>', 'inStock'),
	(93, 'Könyv13', 199, 0, NULL, 'Könyv13<br/><b>Annyira olcsó volt, hogy hamar elfogyott a készlet. Így most nincs.</b>', 'soldOut'),
	(94, 'Könyv14', 999999, 0, NULL, 'Könyv14<br/>Ezt már sajnos nem forgalmazzuk.', 'ranOut'),
	(95, 'Könyv15', 2200, 5, '2016-03-01 00:00:00', 'Könyv16<br/>Kifutott termék', 'ranOut'),
	(96, 'Könyv16', 2000, 1, '2016-03-01 00:00:00', 'Könyv16<br/>Akciós volt 2016-03-01.-ig.', 'inStock');
/*!40000 ALTER TABLE `item` ENABLE KEYS */;


-- Dumping structure for tábla catalog.user
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `password` varchar(64) DEFAULT NULL,
  `rights_level` enum('user','admin') DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table catalog.user: ~2 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `name`, `password`, `rights_level`) VALUES
	(1, 'admin1', '$2y$10$pgfyp0UWEZqwVKdUGfwT.uJPElb9GDzEipr7YXRNQiJSC250c3iLG', 'admin'),
	(2, 'admin2', '$2y$10$u5JyS2tGELcCiY4UkP2gHueBjRV1kx9e9.ndBYCYdhWa1T37Z.WZu', 'admin');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
