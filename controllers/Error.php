<?php
/**
 * Created by PhpStorm.
 * User: ngg
 * Date: 2016.03.22.
 * Time: 11:03
 */

/**
 * Class Error
 *
 * Amennyiben nem létezne az oldal elérhetőségében a kért szolgáltatás.
 *
 */
class Error extends Controller {


    /**
     * Error constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    function index(){

        $this->view->message = 'Nem létezik ez az oldal!';
        $this->view->render('error/index');

    }
}