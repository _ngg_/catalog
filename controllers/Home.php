<?php

/**
 * Created by PhpStorm.
 * User: ngg
 * Date: 2016.03.22.
 * Time: 10:43
 */


const STEP_SIZE = 6;

class Home extends Controller
{


    /**
     * Home constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->view->js = array('home/js/default.js');
    }

    /**
     * A termékek lekéréséért felelős, a model osztályt megkéri, hogy töltsön be STEP_SIZE darab terméket
     * konstans értékének terméket
     *
     * Beállít a nézethez pár változót.
     *
     * @param int $from
     */
    function index($from = 0)
    {

        $from = filter_var($from, FILTER_SANITIZE_NUMBER_INT);

        if ($from < 0) {
            $from = 0;
        }

        $item_model = new Item();

        $this->view->stepSize = STEP_SIZE;
        $this->view->bottom = $from;
        $this->view->top = STEP_SIZE;


        $this->view->items = $item_model->loadItems($from);

        $this->view->render('home/index');

    }

    /**
     * Ajax hívás kiszolgálása.
     *
     *
     * @param int $from
     */
    function indexHttp($from = 0)
    {

        $this->view->bottom = $from;
        $this->view->top = $from + STEP_SIZE;

        $item_model = new Item();

        $result = $item_model->loadItems($from, true);

        echo $result;

    }

    /**
     * Adott termék adatainak lekérése és a nézet beállítása.
     *
     * @param int $id
     * @return bool
     */
    function item($id)
    {

        $id = filter_var($id, FILTER_SANITIZE_NUMBER_INT);

        if ($id < 1) {

            $this->view->message = "Valami nem stimmel az árú azonosítóval!";
            $this->view->render('home/item');
            return false;

        }

        $item = new Item();
        $item = $item->loadItem($id);

        $this->view->message = "";
        $this->view->item = $item;

        $this->view->render('home/item');


    }
}