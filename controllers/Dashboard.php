<?php

/**
 * Created by PhpStorm.
 * User: ngg
 * Date: 2016.03.22.
 * Time: 22:07
 */

/**
 * Class Dashboard
 *
 * Az adminisztrátor felület controller osztálya.
 *
 * Csakis a nézet és model osztály közötti kommunikációra szolgál.
 */
class Dashboard extends Controller
{


    /**
     * Dashboard constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->view->js = array('dashboard' . JAVASCRIPT,
            'dashboard/js/jquery-ui.min.js');
    }

    /**
     * Betölti a megjelenítési fájlt.
     */
    function index()
    {

        $this->view->render('dashboard/index');


    }

    /**
     * Ajax hívás kiszolgálása.
     * A model osztál megkéri, hogy töltse be az adatbázisból az összes terméket.
     *
     */
    function itemsHttp()
    {
        $item = new Item();
        $result = $item->loadAllItems(true);

        echo $result;

    }

    /**
     * Ajax hívás kiszolgálása.
     *
     * Új termék adatait vizsgálja, majd meg kéri a modelt , hogy azt, mentse el az adatbázisban .
     */
    function insertHttp()
    {
        if (isset($_POST["name"]) && isset($_POST["price"]) && isset($_POST["details"])) {
            $data = array(
                "id" => null,
                "name" => $_POST["name"],
                "price" => $_POST["price"],
                "discount" => $_POST["discount"],
                "details" => str_replace(PHP_EOL, "<br/>", $_POST["details"]),
                "state" => Status::RAKTARON,
                "image" => null
            );

            if ($_POST["discount"] > 0) {
                $data["discount_expire_date"] = $_POST["discount_expire_date"];
            } else {
                $data["discount_expire_date"] = null;
            }


            $result = $this->model->newItem($data);

            if(!$result){
                echo 'Hiba történt, próbáld meg később!';
            }
        }

    }


}