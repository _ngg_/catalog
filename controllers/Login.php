<?php
/**
 * Created by PhpStorm.
 * User: ngg
 * Date: 2016.03.22.
 * Time: 10:59
 */

class Login extends Controller
{

    /**
     * Login constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }


    function index()
    {
        //echo 'login';
        $this->view->message = "";
        $this->view->render('login/index');
    }

    function loginProcess()
    {

        $name = filter_var($_POST['user_name'], FILTER_SANITIZE_STRING);
        $password = filter_var($_POST['password'], FILTER_SANITIZE_SPECIAL_CHARS);

        $answer = $this->model->loginProcess($name, $password);

        if ($answer != null || $answer != false) {


            if($answer->getRights() == Rights::ADMIN) {

                header('Location:' . URL . 'dashboard');
            }
            else{
                header('Location:' . URL);

            }
        }else {
            $this->view->message = 'Helytelen adat lett megadva!';
            $this->view->render('login/index');

        }

    }


    function logout()
    {
        $this->model->logout();
    }

}