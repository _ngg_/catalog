<?php

/**
 * Created by PhpStorm.
 * User: ngg
 * Date: 2016.03.22.
 * Time: 22:10
 */

const BASIC_PICTURE = '0.png';
const ITEM_TABLE = 'item';
const IMAGE_TABLE = 'image';

/**
 * Class Item
 *
 * A termékekek model osztálya, itt történik az "üzleti logika" (itt most nincs valami sok) és adatbázis műveletek.
 *
 */
class Item extends Model
{


    private $id;
    private $name;
    private $price;
    private $discount;
    private $discount_expire_date;
    private $details;
    private $state;
    private $images;

    /**
     * Item constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Setter metódus.
     *
     * @param $id
     * @param $name
     * @param $price
     * @param $discount
     * @param $discount_expire_date
     * @param $details
     * @param $state
     * @param $images
     */
    public function setItem($id, $name, $price, $discount, $discount_expire_date, $details, $state, $images)
    {
        $this->id = $id;
        $this->name = $name;
        $this->price = $price;
        $this->discount = $discount;
        $this->discount_expire_date = $discount_expire_date;
        $this->details = $details;
        $this->state = $state;
        $this->images = $images;
    }

    /**
     * Setter metódus a példányhoz.
     *
     * @param $array
     * @param $images
     */
    public function setItemArray($array, $images = null)
    {

        $this->id = $array["id"];
        $this->name = $array["name"];
        $this->price = $array["price"];
        $this->discount = $array["discount"];
        $this->discount_expire_date = $array["discount_expire_date"];
        $this->details = $array["details"];
        $this->state = $array["state"];
        if ($images != null && sizeof($images) > 0) {
            $this->images = $images;
        } else {
            if ($array["image"] == null) {
                $this->images = array(BASIC_PICTURE);
            } else {
                $this->images = array($array["image"]);
            }
        }
    }

    /**
     * (elem+1)-től (elem+1 + STEP_SIZE) -ig lekérdezi a termékeket az adatbázisból.
     *
     * $isJsonAnswerRequired == true esetén @return json_encode($sql_statement)
     *
     * @param int $from
     * @return array Item
     */
    function loadItems($from = 0, $isJsonAnswerRequired = false)
    {
        $sql = 'SELECT * FROM item
                  LEFT JOIN image ON item.id = image.item_id
                    GROUP BY item.id
                    LIMIT ' . $from . ' , ' . STEP_SIZE;

        $result = $this->database->selectSQL($sql, array(), false, true);


        if ($isJsonAnswerRequired) {
            return json_encode($result);
        }

        $items = array();

        foreach ($result as $value) {

            $item = new Item();
            //$images = $item->loadImages($value['id']);

            $item->setItemArray($value);


            array_push($items, $item);

        }


        return $items;

    }

    /**
     * Megkeresi a termékekhez tartozó képeket a termék azonosító alapján.
     *
     * @param $id
     * @return array $name_of_images
     */
    function loadImages($id)
    {

        $data = array('id' => $id);
        $sql = "SELECT image FROM image WHERE item_id = :id";

        $result = $this->database->selectSQL($sql, $data);

        $imgs = array();


        foreach ($result as $img) {

            array_push($imgs, $img["image"]);
        }

        if (sizeof($imgs) == 0) {
            array_push($imgs, BASIC_PICTURE);
        }


        return $imgs;

    }

    /**
     * Adott azonosító alapján megkeresi a terméket.
     *
     * @param int $id
     * @return Item keresett_termék
     */
    function loadItem($id)
    {

        $data = array('id' => $id);

        $sql = 'SELECT * FROM item WHERE id = :id';

        $result = $this->database->selectSQL($sql, $data, true);

        $images = $this->loadImages($id);

        $item = new Item();

        $item->setItemArray($result, $images);

        return $item;

    }

    /**
     * Vissza adja az összes terméket az adatbázisból.
     *
     * @param bool $isJsonAnswerRequired
     * @return array|mixed|string
     */
    function loadAllItems($isJsonAnswerRequired = false)
    {

        $sql = 'SELECT * FROM item';

        $result = $this->database->selectSQL($sql, array());

        if ($isJsonAnswerRequired) {
            return json_encode($result);
        } else {
            return $result;
        }

    }

    /**
     * Az aktuális példányt elmenti az adatbázisban.
     *
     * @return bool|string
     */
    function saveItem()
    {
        try {

            $data = array(
                'name' => $this->name,
                'price' => $this->price,
                'discount' => $this->discount,
                'discount_expire_date' => $this->discount_expire_date,
                'details' => $this->details,
                'state' => Status::RAKTARON
            );

            $this->database->insertSQL('item', $data);

            $item_id = $this->database->lastInsertId();


            return $item_id;

        } catch (PDOException $e) {
            return false;
        }
    }

    /**
     * Elmenti az adatbázisban a képek neveit az aktuális példányhoz.
     *
     * @param $id
     * @param $images_name
     * @return bool
     */
    function saveImages($id, $images_name)
    {

       try {

            foreach ($images_name as $name) {

                $data = array(
                    'item_id' => $id,
                    'image' => $name
                );

                $this->database->insertSQL('image', $data);
            }

            return true;

        } catch (PDOException $e) {

            return false;
        }


    }

    /**
     * Megkeresi a termék fotói közül a legelsőt.
     *
     * @return mixed
     */
    function getCoverImage()
    {
        ksort($this->images);
        return array_values($this->images)[0];
    }



    /* GETTERS - SETTERS */


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param mixed $discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }


    /**
     * @return mixed
     */
    public function getDiscountExpireDate()
    {
        return $this->discount_expire_date;
    }

    /**
     * @param mixed $discount_expire_date
     */
    public function setDiscountExpireDate($discount_expire_date)
    {
        $this->discount_expire_date = $discount_expire_date;
    }

    /**
     * @return mixed
     */
    public function getDetails()
    {
        return $this->details;
    }

    /**
     * @param mixed $details
     */
    public function setDetails($details)
    {
        $this->details = $details;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return array
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param array $images
     */
    public function setImages($images)
    {
        $this->images = $images;
    }

}