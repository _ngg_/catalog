<?php
/**
 * Created by PhpStorm.
 * User: ngg
 * Date: 2016.03.22.
 * Time: 11:30
 */

class Login_model extends Model
{


    /**
     * Login constructor.
     * @param $email
     * @param $password
     */
    public function __construct()
    {
        parent::__construct();
        Session::init();

    }

    /**
     * Ellenőrzi a belépési adatokat és elvégzi a beléptetést.
     *  -Név és jelszó
     *
     * @param $name string
     * @param $password string
     * @return bool
     */
    function loginProcess($name, $password)
    {

        $result = $this->database->selectSQL('SELECT * FROM user WHERE name = :name',
            array('name' => $name), true);


        if (password_verify($password, $result['password'])) {


            Session::set('loggedIn', true);
            $user = new User($name, $result["rights_level"]);
            Session::set('user', $user, true);

            return $user;

        } else {
            return false;
        }
    }






    function logout()
    {
        Session::destroy();
        header('Location:' . URL);
        exit;
    }

}