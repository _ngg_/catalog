<?php

/**
 * Created by PhpStorm.
 * User: ngg
 * Date: 2016.03.25.
 * Time: 18:08
 */

/**
 * Class Status
 *
 * Termék státuszainak meghatározása.
 */

class Status
{

    const
        RAKTARON = "inStock",
        ELFOGYOTT = "soldOut",
        KIFUTOTT = "ranOut";


}