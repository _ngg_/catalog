<?php

/**
 * Created by PhpStorm.
 * User: ngg
 * Date: 2016.03.22.
 * Time: 14:09
 */

/**
 * Class User
 *
 * A felhazsnáló model osztálya.
 * Segít abban, hogy könnyebb megkülönbözteti a felhasználókat és jogosultságaikat egymástól.
 */
class User
{

    private $name;
    private $rights;

    /**
     * User constructor.
     * @param $name
     * @param $right
     */
    public function __construct($name = null, $right = null)
    {
        $this->name = $name;
        $this->rights = $right;
    }

    function setUser($name, $right){
        $this->name = $name;
        $this->rights = $right;
    }

    /**
     * @return null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param null $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return null
     */
    public function getRights()
    {
        return $this->rights;
    }

    /**
     * @param null $rights
     */
    public function setRights($rights)
    {
        $this->rights = $rights;
    }





}