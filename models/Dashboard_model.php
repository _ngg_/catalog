<?php

/**
 * Created by PhpStorm.
 * User: ngg
 * Date: 2016.03.25.
 * Time: 22:54
 */
class Dashboard_model extends Model
{


    /**
     * Dashboard_model constructor.
     */
    public function __construct()
    {
        parent::__construct();

        Session::init();

        Auth::check();
    }


    function newItem($data)
    {

        $item = new Item();
        $item->setItemArray($data);

        $item_id = $item->saveItem();

        if (!$item_id) {

            //adatbázis hiba volt
        } else {

            $img_names = $this->handlingImages($item_id);


            $result = $item->saveImages($item_id, $img_names);

            if(!$result){
                //hiba
                return false;
            }
            else{
                //ok
                return true;
            }

        }


    }

    function handlingImages($item_id)
    {

        $counter = 0;
        $images_name = array();

        foreach ($_FILES as $image) {

            if($image["error"] == 0) {
                $temp = explode(".", $image["name"]);

                $newfilename = $item_id . '_' . $counter . '.' . end($temp);

                move_uploaded_file($image["tmp_name"], getcwd() . '/public/img/' . $newfilename);


                array_push($images_name, $newfilename);

                $counter++;
            }
        }

        return $images_name;

    }


}